package Controller;


import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.ImageCursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerJugador extends Main implements Initializable {


    public Label labelJugador;
    public ImageView btnSalir;
    public Button btnAzul;
    public Button btnRojo;
    public Button btnVerde;
    public Button btnAmarillo;
    public TextField inputNombre;
    public Button btnSeguir;
    @FXML
    private AnchorPane userPane;

    @FXML
    private ImageView userArrow;

    @FXML
    private FontAwesomeIcon iconoRojo;

    @FXML
    private FontAwesomeIcon iconoAzul;

    @FXML
    private FontAwesomeIcon iconoVerde;

    @FXML
    private FontAwesomeIcon iconoAmarillo;

    @FXML
    private FontAwesomeIcon iconoBlanco;
    public int jug = 1;
    private String color = "";

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        iniciar();
    }

    public void iniciar() {
        iconoBlanco.setVisible(false);
        iconoAzul.setVisible(false);
        iconoAmarillo.setVisible(false);
        iconoRojo.setVisible(false);
        iconoVerde.setVisible(false);
        cambiarJug();
    }

    public void onExitButtonClicked(MouseEvent event) {
        gestorJugador.resetJugadores();
        Stage stage = (Stage) this.btnSalir.getScene().getWindow();
        stage.close();
    }

    //---------------------------------------------------------------------
    public void onUserButtonClicked(MouseEvent event) {
        if (userPane.isVisible()) {
            userPane.setVisible(false);
            userArrow.setVisible(false);
            return;
        }
        userPane.setVisible(true);
        userArrow.setVisible(true);
    }

    @FXML
    public void onSaveButtonClicked(ActionEvent event) throws Exception {

        if (inputNombre.getText().equals("") || color.equals("")) {
            labelJugador.setTextFill(Color.RED);
        } else {
            labelJugador.setTextFill(Color.WHITE);
            gestorJugador.crearJugador(inputNombre.getText(), color); // JUGADOR
            gestorCastillo.nuevoCastillo(1, color); // SU CASTILLO
            if (modoDeJuegoSeleccionado.equals("Solo contra IA")) {
                // Crear jugadores robots
            } else {
                switch (color) {
                    case "Rojo": {
                        btnRojo.setDisable(true);
                        break;
                    }
                    case "Verde": {
                        btnVerde.setDisable(true);
                        break;
                    }
                    case "Azul": {
                        btnAzul.setDisable(true);
                        break;
                    }
                    case "Amarillo": {
                        btnAmarillo.setDisable(true);
                        break;
                    }

                }

                if (jug == cantidadDeJugadoresSeleccionado) {
                    Stage salir = (Stage) this.btnSalir.getScene().getWindow();
                    salir.close();
                    Parent root = FXMLLoader.load(getClass().getResource("../assets/fxml/pantallaJuego.fxml"));
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    Image cursor = new Image("images/cursor.png");
                    scene.setCursor(new ImageCursor(cursor));
                    stage.setTitle("Video juego");
                    stage.setX(-2);
                    stage.setY(0);
                    //stage.setFullScreen(true);
                    stage.setScene(scene);
                    stage.show();

                } else {
                    if (jug + 1 == cantidadDeJugadoresSeleccionado) {
                        btnSeguir.setText("JUGAR");
                    }
                    jug++;
                    color = "";
                    inputNombre.setText("");
                    iniciar();
                }
            }


        }


    }


    public void cambiarJug() {
        labelJugador.setText("Jugador " + jug);
    }

    public void seleccionRojo(ActionEvent actionEvent) {
        iconoVerde.setVisible(false);
        iconoAmarillo.setVisible(false);
        iconoBlanco.setVisible(false);
        iconoRojo.setVisible(true);
        iconoAzul.setVisible(false);
        color = "Rojo";
    }

    public void seleccionAzul(ActionEvent actionEvent) {
        iconoVerde.setVisible(false);
        iconoAmarillo.setVisible(false);
        iconoBlanco.setVisible(false);
        iconoRojo.setVisible(false);
        iconoAzul.setVisible(true);
        color = "Azul";
    }

    public void seleccionVerde(ActionEvent actionEvent) {
        iconoVerde.setVisible(true);
        iconoAmarillo.setVisible(false);
        iconoBlanco.setVisible(false);
        iconoRojo.setVisible(false);
        iconoAzul.setVisible(false);
        color = "Verde";
    }

    public void seleccionAmarilla(ActionEvent actionEvent) {
        iconoVerde.setVisible(false);
        iconoAmarillo.setVisible(true);
        iconoBlanco.setVisible(false);
        iconoRojo.setVisible(false);
        iconoAzul.setVisible(false);
        color = "Amarillo";
    }
}
