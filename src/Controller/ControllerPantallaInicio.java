package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerPantallaInicio extends Main implements Initializable {
    public Button btnJugar;
    public Button btnSalir;
    public Pane sceneCantJugadores;
    public Pane sceneModoDeJuego;
    public RadioButton radioJugadores;
    public RadioButton radioCant2;
    public RadioButton radioCant3;
    public RadioButton radioCant4;
    private ToggleGroup groupModoJuego = new ToggleGroup();
    private ToggleGroup groupCantJugadores = new ToggleGroup();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        radioJugadores.setToggleGroup(groupModoJuego);
        radioCant2.setToggleGroup(groupCantJugadores);
        radioCant3.setToggleGroup(groupCantJugadores);
        radioCant4.setToggleGroup(groupCantJugadores);

    }


    public void salirDelJuego(ActionEvent actionEvent) {
        Stage stage = (Stage) btnJugar.getScene().getWindow();
        stage.close();
    }

    public void seleccionDeJuego(ActionEvent actionEvent) {
        sceneModoDeJuego.setVisible(true);
    }

    public void siguientePasoSeleccionJuego(ActionEvent actionEvent) {
        if (groupModoJuego.getSelectedToggle() != null) {
            RadioButton seleccion = (RadioButton) groupModoJuego.getSelectedToggle();
            modoDeJuegoSeleccionado = seleccion.getText();
            sceneCantJugadores.setVisible(true);
            sceneModoDeJuego.setVisible(false);
        }
    }

    public void seleccionarNombreJugadores(ActionEvent actionEvent) {
        if (groupCantJugadores.getSelectedToggle() != null) {
            RadioButton seleccion = (RadioButton) groupCantJugadores.getSelectedToggle();
            cantidadDeJugadoresSeleccionado = Integer.parseInt(seleccion.getText());
            sceneCantJugadores.setVisible(false);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/assets/fxml/PantallaJugador.fxml"));

            Parent root = null;
            try {
                root = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            ControllerJugador controlador = loader.getController();
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.TRANSPARENT);
            scene.setFill(Color.TRANSPARENT);
            stage.setScene(scene);
            stage.showAndWait();
            Stage salir = (Stage) btnJugar.getScene().getWindow();
            salir.close();
        }

    }

    public void cerrarPestana(ActionEvent actionEvent) {
        sceneCantJugadores.setVisible(false);
        sceneModoDeJuego.setVisible(false);
    }
}
