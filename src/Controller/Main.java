package Controller;

import grupo.logica.bl.Casillas_Gema_Power.objeto.Gema;
import grupo.logica.bl.Casillas_Gema_Power.objeto.PowerUp;
import grupo.logica.bl.tropa.productoAbstracto.Tropa;
import grupo.logica.tl.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.ArrayList;

public class Main extends Application {
    /*--------------ATRIBUTOS STATIC DE LA INTERFAZ GRAFICA------------*/
    public static String modoDeJuegoSeleccionado;
    int idsParaMementos = 0;
    public static int cantidadDeJugadoresSeleccionado;
    public int turnoActual = 0;
    public Button btnDesactivarPorDesplegar;
    public int jugador = 0;
    public static Tropa tropaParaMover;
    public static Gema  gemaParaTropa;
    public static PowerUp poderParaTropa;
    public static int cantidadDeMovimientosJugadorActual = 0;
    public static ArrayList<Tropa> tropasEnTablero = new ArrayList<>();
    public static ArrayList<ImageView> tropasEnTableroImagenes = new ArrayList<>();
    public static ArrayList<ImageView> gemasEnTablero = new ArrayList<>();
    public static ArrayList<ImageView> powerUpsEnTablero = new ArrayList<>();
    /*-----------------GESTORES--------------*/
    public static ControllerCastillo gestorCastillo = new ControllerCastillo(1);
    public static ControllerJug gestorJugador = new ControllerJug();
    public static ControllerTropas gestorTropas = new ControllerTropas();
    public static ControllerGema gestorGemas = new ControllerGema();
    public static ControllerCasillaPowerUp gestorPowerUp = new ControllerCasillaPowerUp();
    public static ControllerProxy gestorProxy = new ControllerProxy();
    public static ControllerPowerUpDecorador gestorDecorador = new ControllerPowerUpDecorador();
    public  ControllerMemento gestorMemento = new ControllerMemento();


    @Override
    public void start(Stage stage) throws Exception {

        Parent root = FXMLLoader.load(getClass().getResource("../assets/fxml/PantallaInicio.fxml"));

        Scene scene = new Scene(root);
        //Image cursor = new Image("../src/src/images/cursor.png");
        //scene.setCursor(new ImageCursor(cursor));
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setTitle("Video juego");
        stage.setScene(scene);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
