package Controller;

import grupo.logica.bl.Casillas_Gema_Power.objeto.Gema;
import grupo.logica.bl.Casillas_Gema_Power.objeto.PowerUp;
import grupo.logica.bl.tropa.productoAbstracto.Tropa;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

public class ControllerJuego extends Main implements Initializable {
    /*---COTENEDORES---*/
    public AnchorPane anchorPaneScene;
    public HBox contenedorHbox;
    /*---BARRA LATERAL IZQUIERDA---*/
    public Pane paneBarraIzquierda;
    public Button btnTienda;
    public Circle circuloJugador;
    public Label nombreJugadorPantalla;

    /*---TABLERO---*/
    public static GridPane tablero = new GridPane();
    public AnchorPane anchorPaneTablero;
    public ScrollPane scrollPane;
    /*-----TIENDA-----*/
    public ImageView btnCerrarTienda;
    public Pane sceneTienda;
    public Button btnTerminarTurno;
    public Button btnPersonajes;
    public Button btnDefensas;
    /*---BARRA LATERAL DERECHA---*/
    public Button btnAgregarPersonajeUno;
    public Button btnAgregarPersonajeDos;
    public Button btnAgregarPersonajeTres;

    /*---TIRAR DADO----*/
    public Pane sceneTirarDato;
    public Button btnTirarDado;
    public ImageView imageViewDado;
    public Button btnAceptarDado;
    /*---*/
    public ImageView castilloDelJugadorActual;
    public Label vidaActualDelCastilloJugador;
    public Label oroDelJugadorActual;
    public Pane sceneComprarPersonajes;
    public Button btnCerrar;
    public Button btnComprarEspadachin;
    public Button btnComprarMago;
    public Button btnComprarArquero;
    /*---DESPLIGUE DE TROPAS---*/
    public Pane sceneMisTropas;
    public TableView tableListTropas;
    public TableColumn columNombre;
    public TableColumn columDefensa;
    public TableColumn columAtk;
    public TableColumn columVida;
    public TableColumn columMovimiento;
    public TableColumn columDistancia;
    /*-------------------------*/
    public Label labelSusMovimientos;
    public Button btnConfirmarDesplegar;
    public Button btnComprarJinete;
    public Button btnComprarBersequer;
    public Button btnComprarAsesino;
    public Button btnComprarEspia;
    public Pane sceneVerTropaDeta;
    public Label escudoDeTropa;
    public Label danoDeTropa;
    public Label vidaDeTropa;
    public Label jugadorDetropa;
    public Label personajeDeTropa;
    public Label oroDeTropa;
    public ImageView imgOroDeTropa;
    public Button btnAtacar;
    public Label textoSceneDespligueTropas;
    public Button btnConfirmarAtaque;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*----METODOS DE ARRANQUE-------------*/
        compraDeTropas();

        /*-----GENERAR TABLERO---*/
        generarTablero(); //GENERAR TABLERO
        siguienteTurno(null); //EL PRIMER TURNO

        /*----METODOS ESTABLECIDOS-----*/
        btnCerrarTienda.setOnMouseClicked(event -> {
            sceneTienda.setVisible(false);
        });

        scrollPane.setPannable(true); //TABLERO ARRASTRAR


    }


    public void generarTablero() {
        for (int i = 0; i < 100; i++) {
            for (int j = 0; j < 100; j++) {
                Rectangle rectangle = new Rectangle(100, 100, Color.TRANSPARENT);
                rectangle.setId(i + "," + j);// COLUMNA , FILA
                rectangle.setOnMouseClicked(event -> {

                });
                tablero.add(rectangle, i, j);

            }
        }


        generarCastillos();
        generarGemas();
        generarPowerUps();
        anchorPaneTablero.getChildren().addAll(tablero);
        tablero.setGridLinesVisible(true);

    }

    public void generarPowerUps() {
        /*-----------BUILDER----------*/
        gestorPowerUp.contruccionCasillaPower();
        for (int i = 0; i < gestorPowerUp.obtenerPowerUps().size(); i++) {
            ImageView power = new ImageView(gestorPowerUp.obtenerPowerUps().get(i).getImagen());
            powerUpsEnTablero.add(power);
            PowerUp poderPoner = gestorPowerUp.obtenerPowerUps().get(i);
            poderPoner.setId(i);
            power.setFitHeight(50);
            power.setFitWidth(50);
            tablero.add(power, gestorPowerUp.obtenerPowerUps().get(i).getPosY(), gestorPowerUp.obtenerPowerUps().get(i).getPosX());
            GridPane.setHalignment(power, HPos.CENTER);

            power.setOnMouseClicked(event -> {
                poderParaTropa = poderPoner;
                if (tropaParaMover == null || !gestorProxy.accederDocumento(gestorCastillo.obtenerCastillo(jugador).getColor(), tropaParaMover.getColor())) {
                    Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Selecciona la tropa que deseas que recoja el power up");
                    alerta.setHeaderText(null);
                    alerta.setTitle("PowerUp");
                    alerta.showAndWait();
                } else {
                    if (!tropaParaMover.isDecoradorActivo()) {
                        /*---------MEMENTO--------*/
                        tropaParaMover.setIdMemento(idsParaMementos);
                        idsParaMementos++;
                        gestorMemento.Actualizar_Memento(tropaParaMover.getDefensa(),tropaParaMover.getPuntosAtaque());
                        /*---------DECORADOR--------*/
                        Tropa a = gestorDecorador.decorarPersonaje(tropaParaMover, poderParaTropa);
                        tropaParaMover.setDefensa(a.getDefensa());
                        tropaParaMover.setPuntosAtaque(a.getPuntosAtaque());
                        if (poderParaTropa.getTipo().equals("trampa ataque") || poderParaTropa.getTipo().equals("trampa defensa")) {
                            tropaParaMover.setTrampaActivo(true);
                        }else{
                            tropaParaMover.setDecoradorActivo(true);
                        }
                        tropaParaMover = null;
                        for (int j = 0; j < powerUpsEnTablero.size(); j++) {
                            tablero.getChildren().remove(powerUpsEnTablero.get(poderParaTropa.getId()));
                        }
                    } else {
                        Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Esta tropa ya tiene un power up activo");
                        alerta.setHeaderText(null);
                        alerta.setTitle("PowerUp");
                        alerta.showAndWait();
                    }

                }
            });
        }

    }

    public void generarGemas() {
        /*-----------BUILDER----------*/
        gestorGemas.contruccionGema();
        for (int i = 0; i < gestorGemas.obtenerGemas().size(); i++) {
            ImageView gema = new ImageView(gestorGemas.obtenerGemas().get(i).getImagen());
            gema.setId(String.valueOf(i));
            Gema ponerGema = gestorGemas.obtenerGemas().get(i);
            ponerGema.setId(i);
            gema.setFitHeight(50);
            gema.setFitWidth(50);
            tablero.add(gema, gestorGemas.obtenerGemas().get(i).getPosY(), gestorGemas.obtenerGemas().get(i).getPosX());
            GridPane.setHalignment(gema, HPos.CENTER);
            gemasEnTablero.add(gema);
            gema.setOnMouseClicked(event -> {
                gemaParaTropa = ponerGema;
                if (tropaParaMover == null || !gestorProxy.accederDocumento(gestorCastillo.obtenerCastillo(jugador).getColor(), tropaParaMover.getColor())) {
                    Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Selecciona la tropa que deseas que recoja la gema");
                    alerta.setHeaderText(null);
                    alerta.setTitle("Gema");
                    alerta.showAndWait();
                } else {
                    tropaParaMover.setOroQueLleva(gemaParaTropa.getCantOro());
                    tropaParaMover = null;
                    for (int j = 0; j < gemasEnTablero.size(); j++) {
                        tablero.getChildren().remove(gemasEnTablero.get(gemaParaTropa.getId()));
                    }
                }
            });

        }
    }


    public void generarCastillos() {
        /*---------------PROTOTIPO-----------*/
        for (int i = 0; i < cantidadDeJugadoresSeleccionado; i++) {
            ImageView castillo = new ImageView();
            castillo.setImage(gestorCastillo.obtenerCastillo(i).getImagen());
            castillo.setFitHeight(100);
            castillo.setFitWidth(100);
            castillo.setOnMouseClicked(event -> {
                if (tropaParaMover == null || !gestorProxy.accederDocumento(gestorCastillo.obtenerCastillo(jugador).getColor(), tropaParaMover.getColor())) {
                    Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Selecciona cual tropa va a almacenar oro");
                    alerta.setHeaderText(null);
                    alerta.setTitle("Almacenar oro");
                    alerta.showAndWait();
                } else {
                    int suOro = gestorCastillo.obtenerCastillo(jugador).getCantOro();
                    gestorCastillo.obtenerCastillo(jugador).setCantOro(suOro + tropaParaMover.quitarOro());
                    oroDelJugadorActual.setText(String.valueOf(gestorCastillo.obtenerCastillo(jugador).getCantOro()));

                }

            });
            /*--------------------------*/
            switch (i) {
                case 0:
                    tablero.add(castillo, 0, 0);
                    break;
                case 1:
                    tablero.add(castillo, 99, 99);
                    break;
                case 2:
                    tablero.add(castillo, 99, 0);
                    break;
                case 3:
                    tablero.add(castillo, 0, 99);
                    break;
            }

        }
    }


    /*-----TIENDA------*/

    public void abrirTienda(ActionEvent actionEvent) {
        sceneTienda.setVisible(true);
    }

    public void abrirPersonajes(ActionEvent actionEvent) {
        sceneComprarPersonajes.setVisible(true);
        sceneTienda.setVisible(false);
    }


    public void cerrarPantallaPersonajes(MouseEvent mouseEvent) {
        sceneComprarPersonajes.setVisible(false);
    }

    public void abrirDefensas(ActionEvent actionEvent) {
    }


    /*----SIGUIENTE TURNO------*/
    public void dadoGirando() {
        int numero = (int) (Math.random() * 6) + 1;
        Timer cronometro = new Timer();
        TimerTask acccion = new TimerTask() {
            Image dadoTerminado;

            @Override
            public void run() {
                switch (numero) {
                    case 1:
                        dadoTerminado = new Image("images\\dadoUno.png");
                        break;
                    case 2:
                        dadoTerminado = new Image("images\\dadoDos.png");
                        break;
                    case 3:
                        dadoTerminado = new Image("images\\dadoTres.png");
                        break;
                    case 4:
                        dadoTerminado = new Image("images\\dadoCuatro.png");
                        break;
                    case 5:
                        dadoTerminado = new Image("images\\dadoCinco.png");
                        break;
                    case 6:
                        dadoTerminado = new Image("images\\dadoSeis.png");
                        break;
                }
                imageViewDado.setImage(dadoTerminado);
                btnAceptarDado.setVisible(true);
                btnAceptarDado.setOnMouseClicked(event -> {
                    sceneTirarDato.setVisible(false);
                    cantidadDeMovimientosJugadorActual = numero;
                    labelSusMovimientos.setText(String.valueOf(numero));
                    habilitarBotones();
                });
            }
        };
        cronometro.schedule(acccion, 1000);
    }


    public void siguienteTurno(ActionEvent actionEvent) {
        imageViewDado.setImage(null);
        sceneTirarDato.setVisible(true);
        btnAceptarDado.setVisible(false);
        btnTirarDado.setVisible(true);
        deshabilitarBotones();
        labelSusMovimientos.setText("0");

        btnTirarDado.setOnMouseClicked(event -> {
            btnTirarDado.setVisible(false);
            Image dado = new Image("images\\dado.gif");
            imageViewDado.setImage(dado);
            dadoGirando();
        });

        /*---JUGADOR---*/
        comprobarTropasCompradas();
        comprobarTropasDeplegadas();
        comprobarQuitarPowerUp();
        nombreJugadorPantalla.setText(gestorJugador.obtenerJugadores(turnoActual).getNickname());
        castilloDelJugadorActual.setImage(gestorCastillo.obtenerCastillo(turnoActual).getImagen());
        vidaActualDelCastilloJugador.setText(String.valueOf(gestorCastillo.obtenerCastillo(turnoActual).getVida()));
        oroDelJugadorActual.setText(String.valueOf(gestorCastillo.obtenerCastillo(turnoActual).getCantOro()));
        switch (gestorJugador.obtenerJugadores(turnoActual).getColor()) {
            case "Rojo": {
                circuloJugador.setFill(Color.RED);
                break;
            }
            case "Verde": {
                circuloJugador.setFill(Color.GREEN);
                break;
            }
            case "Azul": {
                circuloJugador.setFill(Color.BLUE);
                break;
            }
            case "Amarillo": {
                circuloJugador.setFill(Color.YELLOW);
                break;
            }
        }

        tropaParaMover = null;
        jugador = turnoActual;
        turnoActual++;

        /*---VALIDACION PARA COMPROBAR QUE EL CICLO SE CUMPLIO---*/
        if (turnoActual == cantidadDeJugadoresSeleccionado) {
            turnoActual = 0;
        }

    }

    public void comprobarQuitarPowerUp() {
        for (int i = 0; i < gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().size(); i++) {
            if(gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).isTrampaActivo()){
                gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).setDefensa(gestorMemento.DevolverMementoDef(gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).getIdMemento()));
                gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).setPuntosAtaque(gestorMemento.DevolverMementoDef(gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).getIdMemento()));
                gestorCastillo.obtenerCastillo(turnoActual).getTropasDelCastillo().get(i).setTrampaActivo(false);
            }
        }
    }

    public void comprobarTropasDeplegadas() {

        if (gestorCastillo.obtenerCastillo(turnoActual).isTropaUnoDesplegada() == false) {
            btnAgregarPersonajeUno.setVisible(true);
        } else {
            btnAgregarPersonajeUno.setVisible(false);
        }
        if (gestorCastillo.obtenerCastillo(turnoActual).isTropaDosDesplegada() == false) {
            btnAgregarPersonajeDos.setVisible(true);
        } else {
            btnAgregarPersonajeDos.setVisible(false);
        }
        if (gestorCastillo.obtenerCastillo(turnoActual).isTropaTresDesplegada() == false) {
            btnAgregarPersonajeTres.setVisible(true);
        } else {
            btnAgregarPersonajeTres.setVisible(false);
        }
    }


    public void comprobarTropasCompradas() {
        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Espadachin") != null) {
            btnComprarEspadachin.setText(" Listo ");
            btnComprarEspadachin.setDisable(true);
        } else {
            btnComprarEspadachin.setText("Comprar");
            btnComprarEspadachin.setDisable(false);
        }

        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Mago") != null) {
            btnComprarMago.setText(" Listo ");
            btnComprarMago.setDisable(true);
        } else {
            btnComprarMago.setText("Comprar");
            btnComprarMago.setDisable(false);
        }

        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Bersequer") != null) {
            btnComprarBersequer.setText(" Listo ");
            btnComprarBersequer.setDisable(true);
        } else {
            btnComprarBersequer.setText("Comprar");
            btnComprarBersequer.setDisable(false);
        }

        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Espia") != null) {
            btnComprarEspia.setText(" Listo ");
            btnComprarEspia.setDisable(true);
        } else {
            btnComprarEspia.setText("Comprar");
            btnComprarEspia.setDisable(false);
        }
        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Asesino") != null) {
            btnComprarAsesino.setText(" Listo ");
            btnComprarAsesino.setDisable(true);
        } else {
            btnComprarAsesino.setText("Comprar");
            btnComprarAsesino.setDisable(false);
        }
        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Jinete") != null) {
            btnComprarJinete.setText(" Listo ");
            btnComprarJinete.setDisable(true);
        } else {
            btnComprarJinete.setText("Comprar");
            btnComprarJinete.setDisable(false);
        }
        if (gestorCastillo.obtenerCastillo(turnoActual).getTropa("Arquero") != null) {
            btnComprarArquero.setText(" Listo ");
            btnComprarArquero.setDisable(true);
        } else {
            btnComprarArquero.setText("Comprar");
            btnComprarArquero.setDisable(false);
        }
    }


    public void actualizarOro() {
        oroDelJugadorActual.setText(String.valueOf(gestorCastillo.obtenerCastillo(jugador).getCantOro()));
    }

    public void deshabilitarBotones() {
        btnTerminarTurno.setVisible(false);
        btnTienda.setDisable(true);
        btnAgregarPersonajeUno.setDisable(true);
        btnAgregarPersonajeDos.setDisable(true);
        btnAgregarPersonajeTres.setDisable(true);

    }

    public void habilitarBotones() {
        btnTerminarTurno.setVisible(true);
        btnTienda.setDisable(false);
        btnAgregarPersonajeUno.setDisable(false);
        btnAgregarPersonajeDos.setDisable(false);
        btnAgregarPersonajeTres.setDisable(false);
        btnTerminarTurno.setVisible(true);
    }

    /*-----------------------COMPRAR PERSONAJES---------------------*/


    public void compraDeTropas() {
        /*--------------FABRICA ABSTRACTA----------*/
        btnComprarEspadachin.setOnMouseClicked(event -> {
            if (comprarTropa(15, 4)) {
                btnComprarEspadachin.setText(" Listo ");
                btnComprarEspadachin.setDisable(true);
            }
        });

        btnComprarMago.setOnMouseClicked(event -> {
            if (comprarTropa(10, 7)) {
                btnComprarMago.setText(" Listo ");
                btnComprarMago.setDisable(true);
            }
        });
        btnComprarArquero.setOnMouseClicked(event -> {
            if (comprarTropa(10, 1)) {
                btnComprarArquero.setText(" Listo ");
                btnComprarArquero.setDisable(true);
            }
        });
        btnComprarBersequer.setOnMouseClicked(event -> {
            if (comprarTropa(25, 3)) {
                btnComprarBersequer.setText(" Listo ");
                btnComprarBersequer.setDisable(true);
            }
        });
        btnComprarJinete.setOnMouseClicked(event -> {
            if (comprarTropa(15, 6)) {

                btnComprarJinete.setText(" Listo ");
                btnComprarJinete.setDisable(true);
            }
        });
        btnComprarAsesino.setOnMouseClicked(event -> {
            if (comprarTropa(5, 2)) {
                btnComprarAsesino.setText(" Listo ");
                btnComprarAsesino.setDisable(true);
            }
        });
        btnComprarEspia.setOnMouseClicked(event -> {
            if (comprarTropa(5, 5)) {
                btnComprarEspia.setText(" Listo ");
                btnComprarEspia.setDisable(true);
            }
        });

    }

    public boolean comprarTropa(int oro, int personaje) {
        int suOro = gestorCastillo.obtenerCastillo(jugador).getCantOro();
        if (suOro >= oro) {
            gestorCastillo.obtenerCastillo(jugador).setCantOro(suOro - oro);
            gestorCastillo.obtenerCastillo(jugador).addTropa(gestorTropas.procesarFuncion(personaje, gestorJugador.obtenerJugadores(jugador).getColor()));
            actualizarOro();
            return true;
        } else {
            oroInsuficiente();
            return false;
        }
    }

    public void oroInsuficiente() {

        Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Oro Insuficiente");
        alerta.setHeaderText(null);
        alerta.setTitle("¡Error!");
        alerta.showAndWait();

    }


    /*-----------------------DESPLEGAR PERSONAJES---------------------*/


    public void agregarTropaAlTableroUno(ActionEvent actionEvent) {
        cargarTableListTropas();
        btnDesactivarPorDesplegar = btnAgregarPersonajeUno;

        btnConfirmarDesplegar.setOnMouseClicked(event -> {
            switch (jugador) {
                case 0:
                    confirmarTropaDesplegar(1, 0);
                    break;
                case 1:
                    confirmarTropaDesplegar(98, 99);
                    break;
                case 2:
                    confirmarTropaDesplegar(98, 0);
                    break;
                case 3:
                    confirmarTropaDesplegar(1, 99);
                    break;

            }

        });
    }

    public void agregarTropaAlTableroDos(ActionEvent actionEvent) {
        cargarTableListTropas();
        btnDesactivarPorDesplegar = btnAgregarPersonajeDos;
        btnConfirmarDesplegar.setOnMouseClicked(event -> {
            switch (jugador) {
                case 0:
                    confirmarTropaDesplegar(2, 0);
                    break;
                case 1:
                    confirmarTropaDesplegar(97, 99);
                    break;
                case 2:
                    confirmarTropaDesplegar(97, 0);
                    break;
                case 3:
                    confirmarTropaDesplegar(2, 99);
                    break;

            }

        });
    }

    public void agregarTropaAlTableroTres(ActionEvent actionEvent) {
        cargarTableListTropas();
        btnDesactivarPorDesplegar = btnAgregarPersonajeTres;
        btnConfirmarDesplegar.setOnMouseClicked(event -> {
            switch (jugador) {
                case 0:
                    confirmarTropaDesplegar(3, 0);
                    break;
                case 1:
                    confirmarTropaDesplegar(96, 99);
                    break;
                case 2:
                    confirmarTropaDesplegar(96, 0);
                    break;
                case 3:
                    confirmarTropaDesplegar(3, 99);
                    break;

            }

        });
    }

    public void cargarTableListTropas() {
        btnConfirmarDesplegar.setVisible(true);
        btnConfirmarAtaque.setVisible(false);
        tableListTropas.getItems().clear();
        textoSceneDespligueTropas.setText("Selecciona la tropa que desea desplegar");
        ObservableList<Tropa> listaDeTropas = FXCollections.observableArrayList();
        tableListTropas.refresh();
        this.columNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
        this.columAtk.setCellValueFactory(new PropertyValueFactory("puntosAtaque"));
        this.columDefensa.setCellValueFactory(new PropertyValueFactory("defensa"));
        this.columVida.setCellValueFactory(new PropertyValueFactory("vida"));
        this.columDistancia.setCellValueFactory(new PropertyValueFactory("alcanceAtaque"));
        this.columMovimiento.setCellValueFactory(new PropertyValueFactory("movimientos"));

        for (int i = 0; i < gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().size(); i++) {
            if (!listaDeTropas.contains(gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().get(i))) {
                if (gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().get(i).isDesplegado() != true) {
                    listaDeTropas.add(gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().get(i));
                }
                tableListTropas.setItems(listaDeTropas);
            }
        }
        sceneMisTropas.setVisible(true);

    }

    public void cerrarSceneMisTropas(ActionEvent actionEvent) {
        sceneMisTropas.setVisible(false);
    }

    public void confirmarTropaDesplegar(int columY, int filaX) {
        if (tableListTropas.getSelectionModel().getSelectedIndex() != -1) {

            Tropa seleccion = (Tropa) tableListTropas.getSelectionModel().getSelectedItem();
            for (int i = 0; i < gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().size(); i++) {
                if (gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().get(i).getNombre().equals(seleccion.getNombre())) {
                    gestorCastillo.obtenerCastillo(jugador).getTropasDelCastillo().get(i).setDesplegado(true);
                }
            }

            ImageView tropa = new ImageView(seleccion.getImagen());
            tropa.setFitWidth(100);
            tropa.setFitHeight(100);

            tablero.add(tropa, columY, filaX);
            tropasEnTablero.add(seleccion);
            tropasEnTableroImagenes.add(tropa);
            seleccion.setX(filaX);
            seleccion.setY(columY);
            btnDesactivarPorDesplegar.setVisible(false);
            if (btnDesactivarPorDesplegar == btnAgregarPersonajeUno) {
                gestorCastillo.obtenerCastillo(jugador).setTropaUnoDesplegada(true);
            }

            if (btnDesactivarPorDesplegar == btnAgregarPersonajeDos) {
                gestorCastillo.obtenerCastillo(jugador).setTropaDosDesplegada(true);
            }

            if (btnDesactivarPorDesplegar == btnAgregarPersonajeTres) {
                gestorCastillo.obtenerCastillo(jugador).setTropaTresDesplegada(true);
            }
            sceneMisTropas.setVisible(false);

            tropa.setOnMouseClicked(event -> {
                tropaParaMover = seleccion;
                sceneVerTropaDeta.setVisible(true);
                vidaDeTropa.setText(String.valueOf(tropaParaMover.getVida()));
                escudoDeTropa.setText(String.valueOf(tropaParaMover.getDefensa()));
                danoDeTropa.setText(String.valueOf(tropaParaMover.getPuntosAtaque()));
                personajeDeTropa.setText(tropaParaMover.getNombre());
                jugadorDetropa.setText(tropaParaMover.getColor());
                imgOroDeTropa.setVisible(false);
                oroDeTropa.setVisible(false);
                /*---------PROXY---------*/
                if (gestorProxy.accederDocumento(gestorCastillo.obtenerCastillo(jugador).getColor(), tropaParaMover.getColor())) {
                    imgOroDeTropa.setVisible(true);
                    oroDeTropa.setVisible(true);
                    oroDeTropa.setText(String.valueOf(tropaParaMover.getOroQueLleva()));
                }
                Timer cronometro = new Timer();
                TimerTask acccion = new TimerTask() {

                    @Override
                    public void run() {
                        sceneVerTropaDeta.setVisible(false);
                    }

                };
                cronometro.schedule(acccion, 5000);

            });
        } else {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Selecciona la tropa que desea desplegar");
            alerta.setHeaderText(null);
            alerta.setTitle("Tropa");
            alerta.showAndWait();

        }

    }

    /*---- ATACAR OTRA TROPA----*/
    public void atacarOtraTropa(ActionEvent actionEvent) {
        if (tropaParaMover == null || !gestorProxy.accederDocumento(gestorCastillo.obtenerCastillo(jugador).getColor(), tropaParaMover.getColor())) {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, "No seleccionaste con cual tropa atacar");
            alerta.setHeaderText(null);
            alerta.setTitle("Atacar");
            alerta.showAndWait();
        } else {
            btnConfirmarDesplegar.setVisible(false);
            btnConfirmarAtaque.setVisible(true);
            tableListTropas.getItems().clear();

            textoSceneDespligueTropas.setText("Selecciona la tropa que desea atacar");
            ObservableList<Tropa> listaDeTropas = FXCollections.observableArrayList();
            tableListTropas.refresh();
            this.columNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
            this.columAtk.setCellValueFactory(new PropertyValueFactory("puntosAtaque"));
            this.columDefensa.setCellValueFactory(new PropertyValueFactory("defensa"));
            this.columVida.setCellValueFactory(new PropertyValueFactory("vida"));
            this.columDistancia.setCellValueFactory(new PropertyValueFactory("alcanceAtaque"));
            this.columMovimiento.setCellValueFactory(new PropertyValueFactory("movimientos"));

            for (int i = 0; i < tropasEnTablero.size(); i++) {
                if (!listaDeTropas.contains(tropasEnTablero.get(i))) {
                    if (!tropasEnTablero.get(i).getColor().equals(gestorCastillo.obtenerCastillo(jugador).getColor())) {
                        listaDeTropas.add(tropasEnTablero.get(i));
                    }
                    tableListTropas.setItems(listaDeTropas);
                }
            }


            sceneMisTropas.setVisible(true);
        }

    }

    public void confirmarAtaque(ActionEvent actionEvent) {
        if (tableListTropas.getSelectionModel().getSelectedIndex() != -1) {
            /*----MEDIADOR----*/
            tropaParaMover.atacar((Tropa) tableListTropas.getSelectionModel().getSelectedItem());
            combrobarMuerte();
            sceneMisTropas.setVisible(false);
        } else {
            Alert alerta = new Alert(Alert.AlertType.INFORMATION, "Selecciona la tropa que desea atacar");
            alerta.setHeaderText(null);
            alerta.setTitle("Atacar");
            alerta.showAndWait();

        }
    }

    public void combrobarMuerte() {
        for (int i = 0; i < tropasEnTablero.size(); i++) {
            if (tropasEnTablero.get(i).isMuerto()) {
                tropaParaMover.setOroQueLleva(tropasEnTablero.get(i).quitarOro());
                tablero.getChildren().remove(tropasEnTableroImagenes.get(i));
                tropasEnTablero.remove(i);
                tropasEnTableroImagenes.remove(i);
            }
        }
    }
}
